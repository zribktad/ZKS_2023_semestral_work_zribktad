package com.coding.exercise.bankapp.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class BankInformation {

	private String branchName;
	
	private Integer branchCode;
	
	private AddressDetails branchAddress;
	
	private Integer routingNumber;



	public boolean isValidBankInformation() {
		// Check if required fields are not null or empty
		return isNotEmpty(branchName)
				&& isValidBranchCode(branchCode)
				&& branchAddress.isValidAddressDetails()
				&& isValidRoutingNumber(routingNumber);
	}

	private boolean isValidBranchCode(Integer branchCode) {
		// Check if branch code has 6 digits
		return branchCode != null && branchCode.toString().length() == 6;
	}

	private boolean isValidRoutingNumber(Integer routingNumber) {
		// Check if routing number has 9 digits
		return routingNumber != null && routingNumber.toString().length() == 9;
	}

	private boolean isNotEmpty(String value) {
		return value != null && !value.trim().isEmpty();
	}


}

