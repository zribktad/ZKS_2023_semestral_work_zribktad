package com.coding.exercise.bankapp.domain;

import lombok.*;

import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class AddressDetails {

	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private String country;
	// Additional helper methods
	private boolean isNotEmpty(String value) {
		return value != null && !value.trim().isEmpty();
	}

	public boolean isValidAddressDetails() {
		// Check if required fields in AddressDetails are not null and meet additional criteria

		return  isNotEmpty(address1)
				&& isNotEmpty(city)
				&& isNotEmpty(state)
				&& isValidZip(zip)
				&& isValidCountry(country);
	}
	private boolean isValidZip(String zip) {
		// Check if zip code has 6 digits
		return zip != null && zip.length() == 5;
	}

	private boolean isValidCountry(String country) {
		// Check if country is in the list of allowed countries
		List<String> allowedCountries = Arrays.asList("USA", "Canada", "UK"); // Add more countries as needed
		return isNotEmpty(country) && allowedCountries.contains(country);
	}

}
