package com.coding.exercise.bankapp.domain;

import lombok.*;

import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class CustomerDetails {

    private String firstName;

    private String lastName;

    private String middleName;

    private Long customerNumber;

    private String status;

    private AddressDetails customerAddress;

    private ContactDetails contactDetails;



    public boolean isValidCustomerDetails() {
        // Check if required fields are not null or empty
        return isNotEmpty(firstName)
                && isNotEmpty(lastName)
                && isValidCostumerNumber(customerNumber)
                && isValidStatus(status)
                && customerAddress.isValidAddressDetails()
                && contactDetails.isValidContactDetails();
    }

// Additional helper methods

    private boolean isValidStatus(String status) {
        // Check if status is either "Active" or "Inactive"
        return "Active".equalsIgnoreCase(status) || "Inactive".equalsIgnoreCase(status);
    }

    private boolean isValidCostumerNumber(Long customerNumber) {
        // Check if customer number is a positive number
        return isNotNull(customerNumber) && customerNumber > 0;
    }

    private boolean isNotEmpty(String value) {
        return value != null && !value.trim().isEmpty();
    }

    private boolean isNotNull(Object value) {
        return value != null;
    }
}
