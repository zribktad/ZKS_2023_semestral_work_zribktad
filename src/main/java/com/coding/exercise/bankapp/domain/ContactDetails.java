package com.coding.exercise.bankapp.domain;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ContactDetails {

	private String emailId;
	
	private String homePhone;

	private String workPhone;

	public boolean isValidContactDetails() {
		// Check if required fields in ContactDetails are not null or empty and meet additional criteria
		return isValidEmail(emailId)
				&& isValidPhoneNumber(homePhone)
				&& isValidPhoneNumber(workPhone);
	}

	private boolean isValidPhoneNumber(String phone) {
		// Check if phone number has a specific format (adjust the regex as needed)
		return isNotEmpty(phone) && phone.length()==10; // Assumes a 10-digit format
	}


	// Additional helper methods

	private boolean isNotEmpty(String value) {
		return value != null && !value.trim().isEmpty();
	}

	private boolean isNotNull(Object value) {
		return value != null;
	}


	private boolean isValidEmail(String email) {
		// Check if email has the "@" symbol and at least one "." (dot) after it
		return isNotEmpty(email) && email.matches(".+@.+\\..+");
	}
}
