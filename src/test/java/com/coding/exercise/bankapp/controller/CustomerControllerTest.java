package com.coding.exercise.bankapp.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.Collections;
import java.util.List;

import com.coding.exercise.bankapp.domain.AddressDetails;
import com.coding.exercise.bankapp.domain.ContactDetails;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.coding.exercise.bankapp.domain.CustomerDetails;
import com.coding.exercise.bankapp.service.BankingServiceImpl;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class CustomerControllerTest {
    @Autowired
    private CustomerController customerController;
    @Autowired
    private BankingServiceImpl bankingService;

    @BeforeEach
    void setUp() {
        customerController.deleteRepository();
        //  bankingService = mock(BankingServiceImpl.class);
        // customerController = new CustomerController();
        //customerController.setBankingService(bankingService);
    }

    @Test
    @Tag("validInputs")
    void testGetAllCustomers() {

        CustomerDetails sampleCustomer = CustomerDetails.builder().firstName("John").lastName("Doe").middleName("M").customerNumber(123L).status("Active").customerAddress(AddressDetails.builder().address1("123 Main St").city("Cityville").state("CA").zip("12345").country("USA").build()).contactDetails(ContactDetails.builder().emailId("john.doe@example.com").homePhone("1234567890").workPhone("9876543210").build()).build();
        // Act
        ResponseEntity<Object> result_tmp = bankingService.addCustomer(sampleCustomer);
        // Assert
        assertEquals(HttpStatus.CREATED, result_tmp.getStatusCode());


        // Act
        List<CustomerDetails> result = customerController.getAllCustomers();

        // Assert
        assertEquals(1, result.size());

    }

    @Test
    @Tag("validInputs")
    void testAddCustomer() {
        // Arrange
        CustomerDetails sampleCustomer = CustomerDetails.builder().firstName("John").lastName("Doe").middleName("M").customerNumber(123L).status("Active").customerAddress(AddressDetails.builder().address1("123 Main St").city("Cityville").state("CA").zip("12345").country("USA").build()).contactDetails(ContactDetails.builder().homePhone("1234567890").workPhone("9876543210").emailId("john.doe@example.com").build()).build();
        // Act
        ResponseEntity<Object> result = bankingService.addCustomer(sampleCustomer);
        // Assert
        assertEquals(HttpStatus.CREATED, result.getStatusCode());
    }

    @Test
    @Tag("invalidInputs")
    void testAddCustomerFailed() {
        // Arrange
        CustomerDetails customer = new CustomerDetails();
        ResponseEntity<Object> result = bankingService.addCustomer(customer);
        // Act and Assert
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    }

    @Test
    @Tag("validInputs")
    void testGetCustomer() {
        // Arrange
        Long customerNumber = 1234L;
        CustomerDetails sampleCustomer = CustomerDetails.builder().firstName("John").lastName("Doe").middleName("M").customerNumber(customerNumber).status("Active").customerAddress(AddressDetails.builder().address1("123 Main St").city("Cityville").state("CA").zip("12345").country("USA").build()).contactDetails(ContactDetails.builder().emailId("john.doe@example.com").homePhone("1234567890").workPhone("9876543210").build()).build();
        bankingService.addCustomer(sampleCustomer);


        // Act
        CustomerDetails result = customerController.getCustomer(customerNumber);

        // Assert
        testCustomerResult(sampleCustomer, result);
    }

    private void testCustomerResult(CustomerDetails sampleCustomer, CustomerDetails result) {
        assertNotNull(result);
        assertEquals(sampleCustomer.getFirstName(), result.getFirstName());
        assertEquals(sampleCustomer.getLastName(), result.getLastName());
        assertEquals(sampleCustomer.getMiddleName(), result.getMiddleName());
        assertEquals(sampleCustomer.getCustomerNumber(), result.getCustomerNumber());
        assertEquals(sampleCustomer.getStatus(), result.getStatus());
        assertEquals(sampleCustomer.getCustomerAddress().getAddress1(), result.getCustomerAddress().getAddress1());
        assertEquals(sampleCustomer.getCustomerAddress().getCity(), result.getCustomerAddress().getCity());
        assertEquals(sampleCustomer.getCustomerAddress().getState(), result.getCustomerAddress().getState());
        assertEquals(sampleCustomer.getCustomerAddress().getZip(), result.getCustomerAddress().getZip());
        assertEquals(sampleCustomer.getCustomerAddress().getCountry(), result.getCustomerAddress().getCountry());

        assertEquals(sampleCustomer.getContactDetails().getEmailId(), result.getContactDetails().getEmailId());
        assertEquals(sampleCustomer.getContactDetails().getHomePhone(), result.getContactDetails().getHomePhone());
        assertEquals(sampleCustomer.getContactDetails().getWorkPhone(), result.getContactDetails().getWorkPhone());
    }

    @Test
    @Tag("validInputs")
    void testUpdateCustomer() {
        // Arrange
        Long customerNumber = 1234L;
        CustomerDetails sampleCustomer = CustomerDetails.builder().firstName("John").lastName("Doe").middleName("M").customerNumber(customerNumber).status("Active").customerAddress(AddressDetails.builder().address1("123 Main St").city("Cityville").state("CA").zip("12345").country("USA").build()).contactDetails(ContactDetails.builder().emailId("john.doe@example.com").homePhone("1234567890").workPhone("9876543210").build()).build();
        bankingService.addCustomer(sampleCustomer);
        CustomerDetails updatedCustomer = CustomerDetails.builder().firstName("Erik").lastName("Doe").middleName("M").customerNumber(customerNumber).status("Active").customerAddress(AddressDetails.builder().address1("123 Main St").city("Cityville").state("CA").zip("12345").country("USA").build()).contactDetails(ContactDetails.builder().emailId("john.doe@example.com").homePhone("1234567890").workPhone("9876543210").build()).build();


        // Act
        ResponseEntity<Object> result = customerController.updateCustomer(updatedCustomer, customerNumber);

        // Assert
        assertEquals(HttpStatus.OK, result.getStatusCode());

    }

    @Test
    @Tag("validInputs")
    void testDeleteCustomer() {
        // Arrange
        Long customerNumber = 1234L;
        CustomerDetails sampleCustomer = CustomerDetails.builder().firstName("John").lastName("Doe").middleName("M").customerNumber(customerNumber).status("Active").customerAddress(AddressDetails.builder().address1("123 Main St").city("Cityville").state("CA").zip("12345").country("USA").build()).contactDetails(ContactDetails.builder().emailId("john.doe@example.com").homePhone("1234567890").workPhone("9876543210").build()).build();
        bankingService.addCustomer(sampleCustomer);

        // Act
        ResponseEntity<Object> result = customerController.deleteCustomer(customerNumber);

        // Assert
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    @Tag("invalidInputs")
    void testGetAllCustomers_EmptyList() {
        // Arrange


        // Act
        List<CustomerDetails> result = customerController.getAllCustomers();

        // Assert
        assertTrue(result.isEmpty());

    }


    @Test
    @Tag("invalidInputs")
    void testGetCustomer_NotFound() {
        // Arrange
        Long customerNumber = 1L;
        // Act
        CustomerDetails result = customerController.getCustomer(customerNumber);
        // Assert
        assertNull(result);
    }


    @Test
    @Tag("invalidInputs")
    void testDeleteCustomer_NotFound() {
        // Arrange
        Long customerNumber = 1L;
        // Act
        ResponseEntity<Object> result = customerController.deleteCustomer(customerNumber);
        // Assert
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());

    }

    @Test
    @Tag("invalidInputs")
    void testDeleteCustomer_BadInput() {
        // Arrange
        Long customerNumber = -1L;
        // Act
        ResponseEntity<Object> result = customerController.deleteCustomer(customerNumber);
        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());

    }

    @ParameterizedTest
    @CsvFileSource(resources = "/BankingApp_CostumerDetail_2way_input.csv", numLinesToSkip = 1, delimiter = ';')
    @Tag("boundaryValues")
    void testCustomerConstructor2Way(String testNumber, String costumerFirstName, String costumerLastName, String costumerNumber, String costumerMiddleName, String costumerStatus, String costumerAddress, String costumerCity, String costumerState, String costumerZIP, String costumerCountry, String costumerPhoneNumber, String costumerEmail, String costumerWorkPhoneNumber, String expectedResult) {
        // Arrange
        costumerFirstName = handleEmptyAndNull(costumerFirstName);
        costumerLastName = handleEmptyAndNull(costumerLastName);
        costumerMiddleName = handleEmptyAndNull(costumerMiddleName);
        costumerStatus = handleEmptyAndNull(costumerStatus);
        costumerAddress = handleEmptyAndNull(costumerAddress);
        costumerCity = handleEmptyAndNull(costumerCity);
        costumerState = handleEmptyAndNull(costumerState);
        costumerZIP = handleEmptyAndNull(costumerZIP);
        costumerCountry = handleEmptyAndNull(costumerCountry);
        costumerPhoneNumber = handleEmptyAndNull(costumerPhoneNumber);
        costumerWorkPhoneNumber = handleEmptyAndNull(costumerWorkPhoneNumber);
        costumerEmail = handleEmptyAndNull(costumerEmail);
        Long number = handleEmptyAndNullForLong(costumerNumber);

        CustomerDetails sampleCustomer = CustomerDetails.builder().firstName(costumerFirstName).lastName(costumerLastName).customerNumber(number).middleName(costumerMiddleName).status(costumerStatus).customerAddress(AddressDetails.builder().address1(costumerAddress).city(costumerCity).state(costumerState).zip(costumerZIP).country(costumerCountry).build()).contactDetails(ContactDetails.builder().homePhone(costumerPhoneNumber).emailId(costumerEmail).workPhone(costumerWorkPhoneNumber).build()).build();
        // Act
        ResponseEntity<Object> result = bankingService.addCustomer(sampleCustomer);
        // Assert
        if ("Failure".equals(expectedResult)) {
            assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        } else {
            assertEquals(HttpStatus.CREATED, result.getStatusCode());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/BankingApp_CostumerDetail_3way_input.csv", numLinesToSkip = 1, delimiter = ';')
    @Tag("boundaryValues")
    void testCustomerConstructor3Way(String testNumber, String costumerFirstName, String costumerLastName, String costumerNumber, String costumerMiddleName, String costumerStatus, String costumerAddress, String costumerCity, String costumerState, String costumerZIP, String costumerCountry, String costumerPhoneNumber, String costumerEmail, String costumerWorkPhoneNumber, String expectedResult) {
        // Arrange
        costumerFirstName = handleEmptyAndNull(costumerFirstName);
        costumerLastName = handleEmptyAndNull(costumerLastName);
        costumerMiddleName = handleEmptyAndNull(costumerMiddleName);
        costumerStatus = handleEmptyAndNull(costumerStatus);
        costumerAddress = handleEmptyAndNull(costumerAddress);
        costumerCity = handleEmptyAndNull(costumerCity);
        costumerState = handleEmptyAndNull(costumerState);
        costumerZIP = handleEmptyAndNull(costumerZIP);
        costumerCountry = handleEmptyAndNull(costumerCountry);
        costumerPhoneNumber = handleEmptyAndNull(costumerPhoneNumber);
        costumerWorkPhoneNumber = handleEmptyAndNull(costumerWorkPhoneNumber);
        costumerEmail = handleEmptyAndNull(costumerEmail);
        Long number = handleEmptyAndNullForLong(costumerNumber);

        CustomerDetails sampleCustomer = CustomerDetails.builder().firstName(costumerFirstName).lastName(costumerLastName).customerNumber(number).middleName(costumerMiddleName).status(costumerStatus).customerAddress(AddressDetails.builder().address1(costumerAddress).city(costumerCity).state(costumerState).zip(costumerZIP).country(costumerCountry).build()).contactDetails(ContactDetails.builder().homePhone(costumerPhoneNumber).emailId(costumerEmail).workPhone(costumerWorkPhoneNumber).build()).build();
        // Act
        ResponseEntity<Object> result = bankingService.addCustomer(sampleCustomer);
        // Assert
        if ("Failure".equals(expectedResult)) {
            assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        } else {
            assertEquals(HttpStatus.CREATED, result.getStatusCode());
        }
    }

    private String handleEmptyAndNull(String value) {
        if ("EMPTY".equals(value)) {
            return "";
        } else if ("null".equals(value)) {
            return null;
        } else {
            return value;
        }
    }

    private Long handleEmptyAndNullForLong(String value) {
        if ("EMPTY".equals(value)) {
            return 0L;
        } else if ("null".equals(value)) {
            return null;
        } else {
            return Long.parseLong(value);
        }
    }
}
