package com.coding.exercise.bankapp.controller;

import com.coding.exercise.bankapp.domain.*;
import com.coding.exercise.bankapp.service.BankingServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AccountControllerTest {
    @Autowired
    private AccountController accountController;
    @Autowired
    private BankingServiceImpl bankingService;

    @BeforeAll
    void setUp() {

        BankInformation bankInformation = createValidBankInformation();

        CustomerDetails sampleCustomer = CustomerDetails.builder().firstName("John").lastName("Doe").middleName("M").customerNumber(123L).status("Active").customerAddress(AddressDetails.builder().address1("123 Main St").city("Cityville").state("CA").zip("12345").country("USA").build()).contactDetails(ContactDetails.builder().emailId("john.doe@example.com").homePhone("1234567890").workPhone("9876543210").build()).build();
        bankingService.addCustomer(sampleCustomer);

        // Create and save sample accounts for the customer
        AccountInformation sourceAccount = AccountInformation.builder()
                .accountType("Savings")
                .bankInformation(bankInformation)
                .accountBalance(5000.0)
                .accountNumber(8L)
                .accountStatus("Active")
                .accountCreated(new Date())
                .build();

        bankingService.addNewAccount(sourceAccount, sampleCustomer.getCustomerNumber());

        AccountInformation destinationAccount = AccountInformation.builder()
                .accountType("Checking")
                .bankInformation(bankInformation)
                .accountBalance(2000.0)
                .accountNumber(9L)
                .accountStatus("Active")
                .accountCreated(new Date())
                .build();

        bankingService.addNewAccount(destinationAccount, sampleCustomer.getCustomerNumber());
    }

    private BankInformation createValidBankInformation() {
        return BankInformation.builder()
                .branchName("Main Branch")
                .branchCode(123456)
                .branchAddress(AddressDetails.builder()
                        .address1("456 Oak St")
                        .city("Townsville")
                        .state("CA")
                        .zip("54321")
                        .country("USA")
                        .build())
                .routingNumber(987654321)
                .build();
    }

    @Test
    @Tag("validInput")
    void testGetByAccountNumber_Success() {
        // Arrange
        Long accountNumber = 8L;

        // Act
        ResponseEntity<Object> result = accountController.getByAccountNumber(accountNumber);

        // Assert
        assertEquals(HttpStatus.FOUND, result.getStatusCode());
    }

    @Test
    @Tag("invalidInput")
    void testGetByAccountNumber_NotFound() {
        // Arrange
        Long accountNumber = 67890L;

        // Act
        ResponseEntity<Object> result = accountController.getByAccountNumber(accountNumber);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }

    @Test
    @Tag("validInput")
    void testAddNewAccount_Success() {
        // Arrange
        Long customerNumber = 123L;
        AccountInformation accountInformation = createValidAccountInformation();

        // Act
        ResponseEntity<Object> result = accountController.addNewAccount(accountInformation, customerNumber);

        // Assert
        assertEquals(HttpStatus.CREATED, result.getStatusCode());

    }

    @Test
    @Tag("invalidInput")
    void testAddNewAccount_ValidationFailure() {
        // Arrange
        Long customerNumber = 67890L;
        AccountInformation invalidAccount = new AccountInformation(); // missing required fields

        // Act
        ResponseEntity<Object> result = accountController.addNewAccount(invalidAccount, customerNumber);

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());

    }


    @Test
    @Tag("validInput")
    void testTransferDetails_Success() {
        // Arrange
        Long customerNumber = 123L;
        TransferDetails transferDetails = createValidTransferDetails();

        // Act
        ResponseEntity<Object> result = accountController.transferDetails(transferDetails, customerNumber);

        // Assert
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertTrue(Objects.requireNonNull(result.getBody()).toString().contains("Success"));
    }

    @Test
    @Tag("invalidInput")
    void testTransferDetails_ValidationFailure() {
        // Arrange
        Long customerNumber = 123L;
        TransferDetails invalidTransfer = new TransferDetails(); // missing required fields


        // Act
        ResponseEntity<Object> result = accountController.transferDetails(invalidTransfer, customerNumber);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());

    }

    @Test
    @Tag("invalidInput")
    void testGetTransactionByAccountNumber_Success() {
        // Arrange
        Long accountNumber = 8L;
        Long customerNumber = 123L;
        //Make money transfer
        TransferDetails transferDetails = createValidTransferDetails();
        ResponseEntity<Object> result_tmp = accountController.transferDetails(transferDetails, customerNumber);
        assertEquals(HttpStatus.OK, result_tmp.getStatusCode());

        // Act
        List<TransactionDetails> result = accountController.getTransactionByAccountNumber(accountNumber);

        // Assert
        assertFalse(result.isEmpty());
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/BankingApp_AccountDetail_2way_input.csv", numLinesToSkip = 1, delimiter = ';')
    @Tag("boundaryValues")
    void testAccountConstructor2Way(String testNumber, String accountNumber, String bankBranchName, String bankBranchCode, String bankAddress, String bankCity, String bankState, String bankZip, String bankCountry, String bankRoutingNumber, String accountStatus, String accountType, String accountBalance, String expectedResult) {
        // Arrange
        Long accNumber = handleEmptyAndNullForLong(accountNumber);
        bankBranchName = handleEmptyAndNull(bankBranchName);
        Integer branchCode = handleEmptyAndNullForInt(bankBranchCode);
        bankAddress = handleEmptyAndNull(bankAddress);
        bankCity = handleEmptyAndNull(bankCity);
        bankState = handleEmptyAndNull(bankState);
        bankZip = handleEmptyAndNull(bankZip);
        bankCountry = handleEmptyAndNull(bankCountry);
        Integer routingNumber = handleEmptyAndNullForInt(bankRoutingNumber);
        accountStatus = handleEmptyAndNull(accountStatus);
        accountType = handleEmptyAndNull(accountType);
        Double accBalance = handleEmptyAndNullForDouble(accountBalance);


        Long customerNumber = 123L;
        AccountInformation accountInformation = AccountInformation.builder()
                .accountType(accountType)
                .bankInformation(BankInformation.builder()
                        .branchName(bankBranchName)
                        .branchCode(branchCode)
                        .branchAddress(AddressDetails.builder()
                                .address1(bankAddress)
                                .city(bankCity)
                                .state(bankState)
                                .zip(bankZip)
                                .country(bankCountry)
                                .build())
                        .routingNumber(routingNumber)
                        .build())
                .accountBalance(accBalance)
                .accountNumber(accNumber)
                .accountStatus(accountStatus)
                .accountCreated(new Date())
                .build();

        // Act
        ResponseEntity<Object> result = accountController.addNewAccount(accountInformation, customerNumber);

        // Assert
        if ("Failure".equals(expectedResult)) {
            assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        } else {
            assertEquals(HttpStatus.CREATED, result.getStatusCode());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/BankingApp_AccountDetail_3way_input.csv", numLinesToSkip = 1, delimiter = ';')
    @Tag("boundaryValues")
    void testAccountConstructor3Way(String testNumber, String accountNumber, String bankBranchName, String bankBranchCode, String bankAddress, String bankCity, String bankState, String bankZip, String bankCountry, String bankRoutingNumber, String accountStatus, String accountType, String accountBalance, String expectedResult) {
        // Arrange
        Long accNumber = handleEmptyAndNullForLong(accountNumber);
        bankBranchName = handleEmptyAndNull(bankBranchName);
        Integer branchCode = handleEmptyAndNullForInt(bankBranchCode);
        bankAddress = handleEmptyAndNull(bankAddress);
        bankCity = handleEmptyAndNull(bankCity);
        bankState = handleEmptyAndNull(bankState);
        bankZip = handleEmptyAndNull(bankZip);
        bankCountry = handleEmptyAndNull(bankCountry);
        Integer routingNumber = handleEmptyAndNullForInt(bankRoutingNumber);
        accountStatus = handleEmptyAndNull(accountStatus);
        accountType = handleEmptyAndNull(accountType);
        Double accBalance = handleEmptyAndNullForDouble(accountBalance);


        Long customerNumber = 123L;
        AccountInformation accountInformation = AccountInformation.builder()
                .accountType(accountType)
                .bankInformation(BankInformation.builder()
                        .branchName(bankBranchName)
                        .branchCode(branchCode)
                        .branchAddress(AddressDetails.builder()
                                .address1(bankAddress)
                                .city(bankCity)
                                .state(bankState)
                                .zip(bankZip)
                                .country(bankCountry)
                                .build())
                        .routingNumber(routingNumber)
                        .build())
                .accountBalance(accBalance)
                .accountNumber(accNumber)
                .accountStatus(accountStatus)
                .accountCreated(new Date())
                .build();

        // Act
        ResponseEntity<Object> result = accountController.addNewAccount(accountInformation, customerNumber);

        // Assert
        if ("Failure".equals(expectedResult)) {
            assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        } else {
            assertEquals(HttpStatus.CREATED, result.getStatusCode());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/BankingApp_TransferDetail_2way_input.csv", numLinesToSkip = 1, delimiter = ';')
    @Tag("boundaryValues")
    void testTransferConstructor2Way(String testNumber, String sourceAccount, String destinationAccount, String amount, String expectedResult) {
        // Arrange
        Long customerNumber = 123L;
       Long toAccountNumber = handleEmptyAndNullForLong(sourceAccount);
        Long fromAccountNumber = handleEmptyAndNullForLong(destinationAccount);
        Double transferAmount = handleEmptyAndNullForDouble(amount);

        TransferDetails transferDetails = TransferDetails.builder()
                .fromAccountNumber(toAccountNumber)
                .toAccountNumber(fromAccountNumber)
                .transferAmount(transferAmount)
                .build();

        // Act
        ResponseEntity<Object> result = accountController.transferDetails(transferDetails, customerNumber);

        // Assert
        if ("Failure".equals(expectedResult)) {
            assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
        } else if ("FailureAmount".equals(expectedResult)) {
            assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        } else {
            assertEquals(HttpStatus.OK, result.getStatusCode());
        }
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/BankingApp_TransferDetail_3way_input.csv", numLinesToSkip = 1, delimiter = ';')
    @Tag("boundaryValues")
    void testTransferConstructor3Way(String testNumber, String sourceAccount, String destinationAccount, String amount, String expectedResult) {
        // Arrange
        Long customerNumber = 123L;
        Long toAccountNumber = handleEmptyAndNullForLong(sourceAccount);
        Long fromAccountNumber = handleEmptyAndNullForLong(destinationAccount);
        Double transferAmount = handleEmptyAndNullForDouble(amount);

        TransferDetails transferDetails = TransferDetails.builder()
                .fromAccountNumber(toAccountNumber)
                .toAccountNumber(fromAccountNumber)
                .transferAmount(transferAmount)
                .build();

        // Act
        ResponseEntity<Object> result = accountController.transferDetails(transferDetails, customerNumber);

        // Assert
        if ("Failure".equals(expectedResult)) {
            assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
        } else if ("FailureAmount".equals(expectedResult)) {
            assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        } else {
            assertEquals(HttpStatus.OK, result.getStatusCode());
        }
    }

    private AccountInformation createValidAccountInformation() {
        AccountInformation accountInformation = AccountInformation.builder()
                .accountType("Savings")
                .bankInformation(createValidBankInformation())
                .accountBalance(500.0)
                .accountNumber(123L)
                .accountCreated(new Date())
                .accountStatus("Active")
                .build();
        accountInformation.setAccountType("Savings");
        // Set other required fields
        return accountInformation;
    }

    private TransferDetails createValidTransferDetails() {
        TransferDetails transferDetails = new TransferDetails();
        transferDetails.setSourceAccount(8L);
        transferDetails.setDestinationAccount(9L);
        transferDetails.setAmount(100.0);
        // Set other required fields
        return transferDetails;
    }

    private TransactionDetails createValidTransactionDetails() {
        TransactionDetails transactionDetails = new TransactionDetails();
        transactionDetails.setTransactionType("Deposit");
        transactionDetails.setAmount(50.0);
        // Set other required fields
        return transactionDetails;
    }

    private String handleEmptyAndNull(String value) {
        if ("EMPTY".equals(value)) {
            return "";
        } else if ("null".equals(value)) {
            return null;
        } else {
            return value;
        }
    }

    private Integer handleEmptyAndNullForInt(String value) {
        if ("EMPTY".equals(value)) {
            return 0;
        } else if ("null".equals(value)) {
            return null;
        } else {
            return Integer.parseInt(value);
        }
    }
    private Long handleEmptyAndNullForLong(String value) {
        if ("EMPTY".equals(value)) {
            return 0L;
        } else if ("null".equals(value)) {
            return null;
        } else {
            return Long.parseLong(value);
        }
    }
    private Double handleEmptyAndNullForDouble(String value) {
        if ("EMPTY".equals(value)) {
            return 0d;
        } else if ("null".equals(value)) {
            return null;
        } else {
            return Double.parseDouble(value);
        }
    }
}
